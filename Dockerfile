FROM node:latest

RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app/

COPY package.json .
RUN npm install

COPY . .

RUN npm run build

EXPOSE 7777

CMD npx vite preview --host 0.0.0.0 --port 7777
